# VisuBin

This is a tool for vizualization of a packing made by the main GA program. It takes as input files avaliable at ```output/packing_info```.

## Requirementes

To run VisuBin, you must install:
* pip3
* Python3

In Debian or Ubuntu this can be done with:
```
$ sudo apt install python3
$ sudo apt install python3-pip
```

You will also need:
```
$ sudo apt-get install python3-tk
```


Then, you can use the following command to get the required libs:
```
$ pip3 install -U numpy
$ pip3 install -U matplotlib
```

## Usage
Copy the files from ```output/packing_data/``` and past them to ```tools/visuBin/input/```. Then, run:
```
python3 visubin.py 
```
The PDF files will be avaliable in: ```tools/visuBin/output/```.
